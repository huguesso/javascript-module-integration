import {getAllProducts} from "./utilis/api.mjs"

window.addEventListener('load',fectchAllProducts)

async function fectchAllProducts(){
    let lisOfHtmlItems = ""
    try {
        let products =  await getAllProducts()
        products.forEach(item => lisOfHtmlItems += addProductItem(item));
        document.getElementById("items").innerHTML = lisOfHtmlItems ;
    } catch (error) {
        console.log(error.message)
        document.getElementById("unavailable_service").style.display="block";
    }
}


function addProductItem(p){
    return `
        <a href="./product.html?id=${p._id}">
            <article>
              <img src="${p.imageUrl}" alt="${p.altTxt}">
              <h3 class="productName">${p.name}</h3>
              <p class="productDescription">${p.description}</p>
            </article>
        </a>
    `
}