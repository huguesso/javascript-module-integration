import {getDetailProduct} from "./utilis/api.mjs"
import { addItem} from "./utilis/cart.mjs"

window.addEventListener('load',fectcProductDetail)
document.getElementById("addToCart").addEventListener('click',addToCart)


async function fectcProductDetail(){
    let productID = getQueryParam("id")
    let colorshtmlItem = `<option value="">--SVP, choisissez une couleur --</option>`
    try {
        let p = await getDetailProduct(productID)
        // if not exist
        if(Object.keys(p).length === 0){
            alert("Product not exist")
            window.location ="./index.html"
        }

        p.colors.forEach(color => colorshtmlItem +=getColorItem(color));

        document.getElementById("image_product").src = p.imageUrl
        document.getElementById("image_product").alt = p.altTxt
        document.getElementById("title").innerHTML = p.name
        document.getElementById("price").innerHTML = p.price
        document.getElementById("description").innerHTML = p.description
        document.getElementById('description').setAttribute('data-product', `${JSON.stringify(p)}`);
        document.getElementById('description').setAttribute('data-id', `${p._id}`);
        document.getElementById("colors").innerHTML = colorshtmlItem
    } catch (error) {
        console.log(error.messge)
        alert("Service unavalaible")
    }
    
}

function getQueryParam(string){
    return new URLSearchParams(window.location.search).get(string)
}

function getColorItem(color){
    return `<option value="${color}">${color}</option>`
}


function addToCart(){
    let color = document.getElementById("colors").value
    let qty = parseInt(document.getElementById("quantity").value)
    let product = document.getElementById('description').getAttribute("data-product")
    const {colors,...result} = JSON.parse(product) ;

    if(!color || qty<0){
        alert("you need to select product color and quantity can't be negatif")
        return ;
    }
    let cart = JSON.parse(localStorage.getItem("KANAP_CART"))   ?? [] ;
    let p = {...result,color,qty}
    cart = addItem(p,cart)
    localStorage.setItem("KANAP_CART",JSON.stringify(cart))
    alert(`product : ${p.name} add to cart`)
    
}