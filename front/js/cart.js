import { getCartTotal, updateItemQty } from "./utilis/cart.mjs";

window.addEventListener('load',displayCart)


function displayCart(){
    let  items = JSON.parse(localStorage.getItem("KANAP_CART"))   ?? [] ;
    refreshLayout(items)
    addEventListener()
}

function updateItemQuantity(event){
    let items = JSON.parse(localStorage.getItem("KANAP_CART"))
    let qty = event.target.value 
    let productID = event.target.getAttribute("data-id");
    let cart = updateItemQty(productID,qty,items)
    localStorage.setItem("KANAP_CART",JSON.stringify(cart))
    refreshLayout(cart)
    addEventListener()
}

function addItemcart(item){
    return `
        <article class="cart__item" data-id="${item._id}" data-color="${item.color}">
                <div class="cart__item__img">
                  <img src="${item.imageUrl}" alt="${item.altTxt}">
                </div>
                <div class="cart__item__content">
                  <div class="cart__item__content__description">
                    <h2>${item.name}</h2>
                    <p>${item.color}</p>
                    <p>${item.price} €</p>
                  </div>
                  <div class="cart__item__content__settings">
                    <div class="cart__item__content__settings__quantity">
                      <p>Qté : ${item.qty}</p>
                      <input type="number" data-id="${item._id}" class="itemQuantity" name="itemQuantity" min="1" max="100" value="${item.qty}">
                    </div>
                    <div class="cart__item__content__settings__delete">
                      <p class="deleteItem">Supprimer</p>
                    </div>
                  </div>
                </div>
        </article>
    `
}


function refreshLayout(items){
    let listHtmlCartItems = ""
    items.forEach(item => {listHtmlCartItems +=addItemcart(item)});
    document.getElementById("cart__items").innerHTML = listHtmlCartItems
    document.getElementById("totalQuantity").innerHTML = items.length
    document.getElementById("totalPrice").innerHTML = getCartTotal(items)
}


function addEventListener(){
    const inputs = document.querySelectorAll('.itemQuantity');
    inputs.forEach((input) => {
        input.addEventListener('change',updateItemQuantity);
    })
}
