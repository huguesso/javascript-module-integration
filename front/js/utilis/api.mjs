import {API_BASE_URL} from "./constants.mjs"

export async function getAllProducts(){
    let requestOptions = {
        method: 'GET',
    };
      
    return fetch(`${API_BASE_URL}/api/products`, requestOptions).then(response => response.json())
}


export async function getDetailProduct(productID){
    let requestOptions = {
        method: 'GET',
    };
      
    return fetch(`${API_BASE_URL}/api/products/${productID}`, requestOptions).then(response => response.json())
}