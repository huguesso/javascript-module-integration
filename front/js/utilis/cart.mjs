export function addItem (p,items) {
  let exist = true
  items.map(item=>{
    if(item._id==p._id && item.color.toLowerCase() == p.color.toLowerCase()){
        exist = false
        item.qty += parseInt(p.qty)
    }
  })

  if(exist){
    items.push(p)
  }

  return items
}

export function removeItem (productID) {
    return items.filter(function (item) {
      return item._id !== productID;
    });
}

export function updateItemQty (productID,qty,items) {
  items.map(item=>{
    if(item._id==productID){
        item.qty = parseInt(qty)
    }
  })
  return  items
}

export function getCartTotal(items) {
    return items.reduce(function(acc, p) { return acc + p.price*p.qty; }, 0)
}

  